PATH := ../../ftdi_cpu_prog/:$(PATH)
PATH := $(HOME)/.qemu/stm32/arm-softmmu/:$(PATH)
FREERTOS_PATH = ../../lib/FreeRTOSv202212.00/FreeRTOS/Source

CROSS_COMPILE = arm-none-eabi-
CC = $(CROSS_COMPILE)gcc
LD = $(CROSS_COMPILE)ld
AR = $(CROSS_COMPILE)ar
AS = $(CROSS_COMPILE)as
OC = $(CROSS_COMPILE)objcopy
OD = $(CROSS_COMPILE)objdump
SZ = $(CROSS_COMPILE)size

CFLAGS = -c -fno-common \
		-ffunction-sections -fdata-sections \
		-Os -mcpu=cortex-m3 \
		-mthumb -g3 -DSTM32F1 -Dsenseor \
        -DSTM32F10X_LD_VL
#		-DSTM32F10X_MD 

LDSCRIPT = ../ld/stm32f103.ld

LIB_OCM3 = ../../lib/opencm3
LDFLAGS	= -lopencm3_stm32f1 --static \
	-Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group \
	-T$(LDSCRIPT) -nostartfiles -Wl,--gc-sections -mthumb \
	-mcpu=cortex-m3 -msoft-float -mfix-cortex-m3-ldrd  \
    -L$(LIB_OCM3)/lib

OCFLAGS	= -Obinary
ODFLAGS	= -S

OUTPUT_DIR = output
TARGET = $(OUTPUT_DIR)/main


INCLUDE = -I../\
	      -I./src/ \
	      -I$(FREERTOS_PATH)/include \
	      -I$(FREERTOS_PATH)/portable/GCC/ARM_CM3/ \
	      -I../common \
          -I$(LIB_OCM3) -I$(LIB_OCM3)/include

SRCS =  ./src/main.c \
	    ../common/freertos_opencm3.c \
	    ../common/usart_opencm3.c \
		$(FREERTOS_PATH)/timers.c \
		$(FREERTOS_PATH)/portable/MemMang/heap_2.c \
		$(FREERTOS_PATH)/portable/GCC/ARM_CM3/port.c \
		$(FREERTOS_PATH)/tasks.c \
		$(FREERTOS_PATH)/queue.c \
		$(FREERTOS_PATH)/croutine.c \
		$(FREERTOS_PATH)/list.c 

OBJS = $(SRCS:.c=.o)

.PHONY : clean all

all: $(TARGET).bin  $(TARGET).list
	@echo "  SIZE $(TARGET).elf"
	$(SZ) $(TARGET).elf

clean:
	@echo "Removing files..."
	rm -rf *.o *.elf *.lst *.out *.bin *.map
	rm -rf ../common/*.o
	rm -rf $(FREERTOS_PATH)/*.o $(FREERTOS_PATH)/include/*.o

$(TARGET).list: $(TARGET).elf
	@echo "  OBJDUMP $(TARGET).list"
	$(OD) $(ODFLAGS) $< > $(TARGET).lst

$(TARGET).bin: $(TARGET).elf
	@echo "  OBJCOPY $(TARGET).bin"
	$(OC) $(OCFLAGS) $(TARGET).elf $(TARGET).bin

$(TARGET).elf: $(OBJS) 
	@echo "  LD $(TARGET).elf"
	$(CC)  -o $(TARGET).elf $(OBJS) $(LDFLAGS)

%.o: %.c
	@echo "  CC $<"
	$(CC) $(INCLUDE) $(CFLAGS)  $< -o $*.o

%.o: %.S
	@echo "  CC $<"
	$(CC) $(INCLUDE) $(CFLAGS)  $< -o $*.o

flash: output/main.bin
	stm32flash.sh -w output/main.bin /dev/ttyUSB0

qemu: output/main.bin
	qemu-system-arm -M stm32-p103 -serial stdio -serial stdio -serial stdio -kernel output/main.bin

listen:
	sleep 1
	minicom -D /dev/ttyUSB0
