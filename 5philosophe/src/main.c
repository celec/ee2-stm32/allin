#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "common.h"
#include "queue.h"
#include "croutine.h"

#define NB_PHILO 10
#define DELAY 100
#define LONG_MSG

int eat[NB_PHILO];
#ifndef LONG_MSG 
xSemaphoreHandle xMutex[NB_PHILO];
#else // the last MUTEX is for UART long messages
xSemaphoreHandle xMutex[NB_PHILO + 1];
#endif

#ifdef LONG_MSG
void usartPhilo (int n, char *s) {
    if (!(xSemaphoreTake(xMutex[NB_PHILO], DELAY / portTICK_RATE_MS) == pdFALSE)) {
        uart_puts("Philosopher \0");
        uart_putc('1' + n);
        uart_puts(s);
        xSemaphoreGive(xMutex[NB_PHILO]);
    }
}
#endif

void fsm (void *p) {
    int number = *(int *)p;
    while (eat[number] != 1) {

        // 1: get the right chopstick
        #ifndef LONG_MSG 
        uart_putc('a' + number);
        #else
        usartPhilo(number, " is hungry!\r\n\0");
        #endif
        vTaskDelay(DELAY / portTICK_RATE_MS);
        // when the chopsticks are already used
        if (xSemaphoreTake(xMutex[number], DELAY / portTICK_RATE_MS) == pdFALSE) {
            #ifndef LONG_MSG 
            uart_putc('U' + number);
            #else
            usartPhilo(number, " failed to take a chopstick. \0");
            #endif
        } else {
            vTaskDelay(DELAY / portTICK_RATE_MS);
            #ifdef LONG_MSG
            usartPhilo(number, " have the 1st chopstick!\r\n\0");
            #endif

            // if all the philosophers have a chopstick each
            if (xSemaphoreTake(xMutex[(number + 1) % NB_PHILO], DELAY / portTICK_RATE_MS) == pdFALSE) {
                #ifndef LONG_MSG 
                uart_putc('u' + number);
                #else
                usartPhilo(number, " had the 1st chopstick but had to release it. \0");
                #endif
                xSemaphoreGive(xMutex[number]);
            } else { // 2: get the left chopstick & 3: eating
                #ifndef LONG_MSG 
                uart_putc('A' + number);
                #else
                usartPhilo(number, " has both chopsticks ; he's eating!!\r\n\0");
                #endif
                vTaskDelay(DELAY / portTICK_RATE_MS);

                // 4: release both chopsticks as eating is ended
                xSemaphoreGive(xMutex[number]);
                xSemaphoreGive(xMutex[(number + 1) % NB_PHILO]);
                #ifndef LONG_MSG 
                uart_putc('0' + number);
                #else
                usartPhilo(number, " has finished eating ; he's resting.\r\n\0");
                #endif

                // 5: idle
                eat[number] = 1;
            }
        }
    }

    while (1) vTaskDelay(100 / portTICK_RATE_MS); // idle when FSM is ended
}

int main(void) { 
    static int p[NB_PHILO];
    int i;
    Usart1_Init();
    Led_Init();
    
    for (i = 0; i < NB_PHILO; i++) {
        xMutex[i] = xSemaphoreCreateMutex(); 
        p[i] = i;
        eat[i] = 0;
    }
    #ifdef LONG_MSG
    xMutex[NB_PHILO] = xSemaphoreCreateMutex();
    #endif
    for (i = 0; i < NB_PHILO; i++) xTaskCreate(fsm, (const char *)&i, STACK_BYTES(256), (void *)&p[i], 1, 0);
    vTaskStartScheduler();
    
    while (1);
    
    return 0;
}
