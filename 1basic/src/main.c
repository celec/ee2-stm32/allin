#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "common.h"

void display_dec(unsigned long c){
	unsigned long v, k;
	k = 1e6; 
	while (k > 0){
		v = c / k;
		c -= (v * k);
		k /= 10;
		uart_putc((unsigned char)v + '0');
	}
}	

void vLedsFloat (void* dummy) {
    while (1) {
        Led_Hi1(); vTaskDelay(120/portTICK_RATE_MS);
        Led_Lo1(); vTaskDelay(120/portTICK_RATE_MS);
    }
}

void vLedsFlash (void* dummy) {
    while (1) {
        Led_Hi2(); vTaskDelay(301/portTICK_RATE_MS);
        Led_Lo2(); vTaskDelay(301/portTICK_RATE_MS);
    }
}

void vPrintUart (void* dummy) {
    portTickType last_wakeup_time;
    last_wakeup_time = xTaskGetTickCount();
    
    while (1) { 
        uart_puts("Hello World\r\n");
        uart_puts("Stack: ");
        display_dec(uxTaskGetStackHighWaterMark(NULL));
        uart_puts("\r\n");
	    vTaskDelayUntil(&last_wakeup_time, 500/portTICK_RATE_MS);
    }
}

void vPrintList (void* dummy) {
    char buff[160]; // 40 * 4
    portTickType last_wakeup_time;
    last_wakeup_time = xTaskGetTickCount();

    vTaskList(buff);
    while (1) { 
        uart_puts(buff);
	    vTaskDelayUntil(&last_wakeup_time, 500/portTICK_RATE_MS);
    }
}

void vApplicationStackOverflowHook (TaskHandle_t xTask, char *pTaskName) {
    uart_puts(pTaskName);
    uart_puts(" OFd\r\n");
}

int main (void) {
    Usart1_Init(); // inits clock as well
    Led_Init();
    Led_Hi1();

    if (!(pdPASS == xTaskCreate(vLedsFloat, (const char*) "LedFloat",   64,  NULL, 1,  NULL))) goto hell;
    if (!(pdPASS == xTaskCreate(vLedsFlash, (const char*) "LedFlash",   64,  NULL, 2,  NULL))) goto hell;
    if (!(pdPASS == xTaskCreate(vPrintUart, (const char*) "Uart",       64,  NULL, 3,  NULL))) goto hell;
    if (!(pdPASS == xTaskCreate(vPrintList, (const char*) "List",       256, NULL, 4,  NULL))) goto hell;

    vTaskStartScheduler();

    hell: // should never be reached
	    while (1);
        return 0;
}
